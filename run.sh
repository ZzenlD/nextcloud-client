#!/bin/sh
# shellcheck disable=SC2039

while true; do
  nextcloudcmd "$( [[ "${NEXTCLOUD_TRUST_INVALID_CERTIFICATE}" == "yes" ]] && echo "--trust" )" --non-interactive -u "${NEXTCLOUD_USERNAME}" -p "${NEXTCLOUD_PASSWORD}" /nextcloud "${NEXTCLOUD_URL}"
  sleep "${NEXTCLOUD_SYNC_INTERVAL}"
done
